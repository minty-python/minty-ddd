# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import copy
from minty.cqrs import (
    CommandBase,
    Event,
    EventService,
    QueryBase,
    UserInfo,
    event,
)
from minty.entity import EntityBase
from minty.repository import RepositoryFactory
from unittest import mock
from uuid import uuid4


class DummyCommand(CommandBase):
    pass


class DummyQuery(QueryBase):
    pass


class DummyEntity(EntityBase):
    def entity_id(self):
        return "yolo"

    def __init__(self, event_service):
        self.event_service = event_service
        self.nothing = None
        self.some_value = 42
        self.some_string = "str"
        self.some_field = "some_value"

    @event("Dummied", extra_fields=["some_field"])
    def some_action(self, some_value):
        self.nothing = 3
        self.some_value = some_value
        self.some_string = "lalala"


class TestUserInfo:
    def test_userinfo(self):
        user_id = uuid4()

        user_info = UserInfo(
            user_uuid=user_id, permissions={"test": True, "production": False}
        )

        assert user_info.user_uuid == user_id
        assert user_info.permissions == {"test": True, "production": False}


class TestEventDecorator:
    def test_event_decorator(self):
        correlation_id = uuid4()
        user_uuid = uuid4()

        es = EventService(correlation_id, "d", "c", user_uuid)
        e = DummyEntity(es)

        e.some_action(666)

        assert e.some_value == 666
        assert len(es.event_list) == 1

        assert isinstance(es.event_list[0], Event)
        assert es.event_list[0].correlation_id == correlation_id
        assert es.event_list[0].domain == "d"
        assert es.event_list[0].context == "c"
        assert es.event_list[0].user_uuid == user_uuid
        assert es.event_list[0].entity_type == "DummyEntity"
        assert es.event_list[0].entity_id == e.entity_id
        assert es.event_list[0].event_name == "Dummied"
        assert es.event_list[0].changes == [
            {"key": "nothing", "new_value": 3, "old_value": None},
            {"key": "some_value", "new_value": 666, "old_value": 42},
            {"key": "some_string", "new_value": "lalala", "old_value": "str"},
        ]
        assert es.event_list[0].entity_data == {"some_field": "some_value"}


class TestCQRSBaseClasses:
    def test_command_base(self):
        mock_repo_factory = mock.MagicMock(spec=RepositoryFactory)
        mock_repo_factory.get_repository.return_value = "repo"
        event_service = EventService(uuid4(), "d", "c", uuid4())

        c = DummyCommand(
            repository_factory=mock_repo_factory,
            context="c",
            user_uuid=uuid4(),
            event_service=event_service,
        )
        r = c.get_repository("foo")

        assert r == "repo"
        mock_repo_factory.get_repository.assert_called_once_with(
            name="foo",
            context="c",
            event_service=event_service,
            read_only=False,
        )

    def test_query_base(self):
        mock_repo_factory = mock.MagicMock(spec=RepositoryFactory)
        mock_repo_factory.get_repository.return_value = "repo"

        c = DummyQuery(
            repository_factory=mock_repo_factory,
            context="c",
            user_uuid=uuid4(),
        )
        r = c.get_repository("foo")

        assert r == "repo"
        mock_repo_factory.get_repository.assert_called_once_with(
            name="foo", context="c", event_service=None, read_only=True
        )


class TestEventService:
    def setup_method(self):
        user_uuid = uuid4()
        self.event = Event(
            uuid=uuid4(),
            created_date="",
            correlation_id=uuid4(),
            domain="TestDomain",
            context="dev.zaaksysteem.development",
            user_uuid=user_uuid,
            user_info=UserInfo(user_uuid=user_uuid, permissions={}),
            entity_type="TestEntity",
            entity_id=uuid4(),
            event_name="TestEventCreated",
            changes=[],
            entity_data={},
        )
        self.event_service = EventService(
            correlation_id=uuid4(),
            domain="TestDomain",
            context="dev.zaaksysteem.development",
            user_uuid=uuid4(),
        )

    def test_format_changes(self):
        event = copy.deepcopy(self.event)
        event.changes = [
            {"key": "title", "new_value": "new_title", "old_value": None},
            {"key": "phase", "new_value": 2, "old_value": None},
            {"key": "completed", "new_value": False, "old_value": None},
            {"key": "user_defined", "new_value": True, "old_value": None},
            {
                "key": "description",
                "new_value": "some description",
                "old_value": None,
            },
        ]
        changes = event.format_changes()
        assert changes == {
            "completed": False,
            "description": "some description",
            "phase": 2,
            "title": "new_title",
            "user_defined": True,
        }

    def test_previous_value(self):
        event = copy.deepcopy(self.event)
        event.changes = [
            {
                "key": "title",
                "new_value": "new_title",
                "old_value": "old_title",
            },
            {"key": "phase", "new_value": 2, "old_value": None},
            {"key": "completed", "new_value": False, "old_value": None},
            {"key": "user_defined", "new_value": True, "old_value": None},
            {
                "key": "description",
                "new_value": "some description",
                "old_value": None,
            },
        ]
        assert event.previous_value("title") == "old_title"

    def test_new_value(self):
        event = copy.deepcopy(self.event)
        event.changes = [
            {
                "key": "title",
                "new_value": "new_title",
                "old_value": "old_title",
            },
            {"key": "phase", "new_value": 2, "old_value": None},
            {"key": "completed", "new_value": False, "old_value": None},
            {"key": "user_defined", "new_value": True, "old_value": None},
            {
                "key": "description",
                "new_value": "some description",
                "old_value": None,
            },
        ]
        assert event.new_value("title") == "new_title"

    def test_get_event_by_type(self):
        event_service = copy.deepcopy(self.event_service)

        event_1 = copy.deepcopy(self.event)
        event_2 = copy.deepcopy(self.event)
        event_3 = copy.deepcopy(self.event)

        event_1.entity_id = uuid4()
        event_2.entity_id = uuid4()
        event_3.entity_id = uuid4()

        event_3.entity_type = "somethingelse"

        event_service.event_list = [event_1, event_2, event_3]
        events = event_service.get_events_by_type(entity_type="TestEntity")

        assert len(events) == 2
        for ev in events:
            if ev.entity_id == event_3.entity_id:
                raise AssertionError()
            elif ev.entity_id in [event_1.entity_id, event_2.entity_id]:
                assert True
