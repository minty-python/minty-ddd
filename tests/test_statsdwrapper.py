# SPDX-FileCopyrightText: xxllnc Zaakgericht BV
#
# SPDX-License-Identifier: EUPL-1.2

import minty
import statsd


def test_statsd_wrapper():
    w = minty.StatsdWrapper()
    minty.STATSD_PREFIX = "prefix"

    w.context = "some_context"

    t = w.get_timer("timer_name")
    assert isinstance(t, statsd.Timer)
    assert t.name == "prefix.some_context.timer_name"

    c = w.get_counter("counter_name")
    assert isinstance(c, statsd.Counter)
    assert c.name == "prefix.some_context.counter_name"

    g = w.get_gauge("gauge_name")
    assert isinstance(g, statsd.Gauge)
    assert g.name == "prefix.some_context.gauge_name"


def test_statsd_wrapper_nocontext():
    w = minty.StatsdWrapper()
    minty.STATSD_PREFIX = "prefix"

    w.context = None

    t = w.get_timer("timer_name")
    assert isinstance(t, statsd.Timer)
    assert t.name == "prefix.all.timer_name"

    c = w.get_counter("counter_name")
    assert isinstance(c, statsd.Counter)
    assert c.name == "prefix.all.counter_name"

    g = w.get_gauge("gauge_name")
    assert isinstance(g, statsd.Gauge)
    assert g.name == "prefix.all.gauge_name"
